// mem_mng.h
#ifndef __MEM_MNG_H__
#define __MEM_MNG_H__

#include "double_list.h"


#include <stdint.h>
#include "cassert.h"
#include "common_cfg.h"

//#define MSG_BUF_SIZE       (50u)

#define MSG_FIFO_NUM       (15u)
#define TRANST_FIFO_NUM    (40u)

struct mem_msg_block_t {
    struct dl_list node;
	uint8_t buf[50];
};

struct mem_trans_block_t {
    struct dl_list node;
	uint8_t buf[MTU_SIZE];
	uint8_t head[PDU_HEAD_RSEV_SIZE];
};


void mem_msg_list_init(void);

struct mem_msg_block_t* mem_msg_get_block(void);


void mem_msg_return_block(struct mem_msg_block_t * item);

uint8_t mem_is_msg_range_addr(struct mem_msg_block_t * item);



///-------------------------------///////
void mem_trans_list_init(void);
struct mem_trans_block_t* mem_trans_tx_get_block(void);
struct mem_trans_block_t* mem_trans_get_block(void);
void mem_trans_return_block(struct mem_trans_block_t * item);

void mem_trans_return_all_tx_block(void);
struct mem_trans_block_t * mem_trans_return_one_tx_block(struct mem_trans_block_t *item);

uint8_t mem_is_trans_range_addr(struct mem_trans_block_t * item);


///------------------------------////////

void mem_init(void);



#endif




