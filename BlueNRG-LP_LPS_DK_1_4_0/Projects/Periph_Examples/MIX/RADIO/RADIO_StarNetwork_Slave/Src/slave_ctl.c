#include "usr_cfg.h"
#include "slave_ctl.h"
#include "rf_driver_ll_radio_2g4.h"
#include "rf_driver_hal_vtimer.h"
#include "cassert.h"
#include "sha256.h"
#include "mac_mng.h"
#include "ull_chan.h"
#include "lll_chan.h"
#include "ll_pdu.h"
#include "radio_event.h"
#include "mem_mng.h"


#define MASK_INTERRUPTS() __disable_irq(); 
#define UNMASK_INTERRUPTS() __enable_irq();

static ActionPacket dumbPacket;
static ActionPacket scanPacket;
static ActionPacket syncPacket;
// node connection 
static ActionPacket rxPacket;
// create connection
static ActionPacket conReqPacket;
// connection confirm 
static ActionPacket conRspPacket;

// tx data
static ActionPacket txPacket;

static uint32_t next_wakeup_time = 0;

static uint8_t tx_data[MAX_PACKET_LENGTH];
static uint8_t con_req[MAX_PACKET_LENGTH];
static uint8_t con_rsprx[MAX_PACKET_LENGTH];

// connection handle. from master. should be less than (NODE_NUMBER + 1)
static uint16_t con_handle = 0;

static uint8_t con_sta = DISCONNECTED;

static uint32_t dis_con_count = 0;
static uint8_t radio_sta = STA_UNINIT;
static uint8_t channel = 16;   /* Start Channel */
static uint32_t access_address = ADV_ACCESS_ADDRESS;

static uint32_t scan_interval = DEFUALT_SCAN_INTERVAL;  	//unit:us
//static uint32_t scan_interspace = DEFUALT_SCAN_INTERSPACE;  //unit:ticks
static uint8_t scan_chan_idex = 0;
static uint8_t scan_chan_map[3] = DEFUALT_SCAN_CHAN_MAP;

static uint64_t con_timestap = 0;
static uint16_t cur_event_num = 0;

static queue_msg_t *pmsg_ctl;
static queue_msg_t *pmsg_ctl_rsp;

static queue_msg_t *pmsg_report;

static struct mem_trans_block_t *prx_block;
static struct mem_trans_block_t *psync_block;
static struct mem_trans_block_t *ptx_block;
static struct mem_trans_block_t *ptx_empty_block;

static msg_t cur_txmsg;


static scmd_t* pscmd;
static scmd_head_t shead;


uint8_t bdaddr[6];
uint8_t peer_bdaddr[6];


static void update_con_chan(void);
static uint8_t ctl_msg_schedule(queue_msg_t *msg_head);


static uint8_t select_scan_chan(void)
{
  scan_chan_idex++;
  if(scan_chan_idex >=3)
  	scan_chan_idex = 0;
  COMPrintf("scan_chan_idex:%d \r\n", scan_chan_idex);
  RADIO_SetChannel(STATE_MACHINE_0, scan_chan_map[scan_chan_idex], 0);
  return scan_chan_idex;
}

static uint8_t select_sync_chan(void)
{
  uint8_t index = cur_event_num % 3;
  RADIO_SetChannel(STATE_MACHINE_0, scan_chan_map[index], 0);
  return index;
}

static uint8_t dumbConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  RADIO_TIME_UNIT;
  scanPacket.WakeupTime = next_wakeup_time; 
  select_scan_chan();
  RADIO_SetGlobalReceiveTimeout(scan_interval);
  RADIO_SetReservedArea(&scanPacket);
  return TRUE; 
}

static uint8_t advertisingPacketCheck(ActionPacket* p)
{ 
  if((p->data[1]) >= sizeof(adv_packet_t)){
    adv_packet_t* padv = (adv_packet_t*)(&(p->data[2]));
	if(padv->discoverable > 0){
		cur_event_num = padv->cur_event_num;
		
		return TRUE;
	}
  }
  
  return FALSE;
}

static void __do_scan(void)
{
	next_wakeup_time = HAL_VTIMER_GetCurrentSysTime() + 10*RADIO_TIME_UNIT;
	scanPacket.WakeupTime = next_wakeup_time;
	RADIO_SetGlobalReceiveTimeout(scan_interval);
	access_address = ADV_ACCESS_ADDRESS;
	RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
	select_scan_chan();
	radio_sta = STA_IDLE;
	RADIO_SetReservedArea(&scanPacket);
}



static void __do_create_connection(ActionPacket* p)
{
    //RADIO_SetGlobalReceiveTimeout(scan_interspace);	
	con_sta = CONNECTING;
	
	uint64_t cur_time;
	con_timestap = TIMER_GetAnchorPoint(&cur_time);
	
	
}



static uint8_t scanConditionRoutine(ActionPacket* p)
{
  uint8_t ret;

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
    /* received a packet */
    if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
	  //COMPrintf("xxx \r\n");
	  ret = advertisingPacketCheck(p);
	  if(ret){
	  	__do_create_connection(p);
	    return TRUE; 
	  }else{
	    __do_scan();
	    return FALSE; 
	  }
    }
  }
  __do_scan();
  return FALSE; 
}


static uint8_t scanDataRoutine(ActionPacket* p,  ActionPacket* next)
{

  if(next == &conReqPacket){
  	//COMPrintf("__do_create_connection\r\n");
  	adv_packet_t* padv = (adv_packet_t*)(&(p->data[2]));
  	memcpy(peer_bdaddr, padv->dev_addr, sizeof(peer_bdaddr));

	con_req_t *pcon_req = (con_req_t*)&con_req[2];
	pcon_req->cmd = CMD_CONNECTION_REQ;
	memcpy(pcon_req->dev_addr, bdaddr, sizeof(bdaddr));
	con_req[1] = sizeof(con_req_t);

	
    COMPrintf("cur_event_num:%d \r\n", cur_event_num);
  }else if(next == &scanPacket){
  // clear all msg in fifo
    
    for(int i=0;i<10;i++)
	{
	    uint8_t ret = ctl_msg_schedule(pmsg_ctl);
		if(ret != OS_ERR_Q_EMPTY){
	       continue;
		}else{
           break;
		}
    }
  }

  return TRUE;
}

static uint8_t conReqConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  RADIO_TIME_UNIT;
  RADIO_SetGlobalReceiveTimeout(DEFUALT_RECEIVETIMEOUT);
  //RADIO_SetGlobalReceiveTimeout(DEFUALT_RECEIVETIMEOUT);
  return TRUE; 
}
static uint8_t conReqDataRoutine(ActionPacket* p,  ActionPacket* next)
{
  return TRUE;
}

static uint8_t checkRspPacket(ActionPacket* p)
{

  if(p->data[1] == sizeof(con_rsp_t)){
	con_rsp_t *prsp = (con_rsp_t*)(&p->data[2]);
	if(memcmp(prsp->dst_dev_addr, bdaddr, sizeof(bdaddr)) == 0)
	{		
		return TRUE; 
	}
  }

  return FALSE;
}
static uint8_t conRspConditionRoutine(ActionPacket* p)
{

    if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
		/* received a packet */
		if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
		  if(checkRspPacket(p)){
			  next_wakeup_time = con_timestap + 3*RADIO_TIME_UNIT;
			  rxPacket.WakeupTime = next_wakeup_time - NODES_RX_OFFSET; 
			  RADIO_SetReservedArea(&rxPacket);
			  access_address = DEF_CON_ACCESS_ADDRESS;
			  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
			  update_con_chan();
			  return TRUE;
		  }
		}
    }
  __do_scan();
  return FALSE;
}
static uint8_t conRspDataRoutine(ActionPacket* p,  ActionPacket* next)
{
  //COMPrintf_hexdump(p->data,p->data[1]+2);
  if((next == &rxPacket) && (con_sta == CONNECTING)){
    con_sta = CONNECTED;
	con_rsp_t *prsp = (con_rsp_t*)(&p->data[2]);
	con_handle = prsp->con_handle;
	
	
	onconnect_t *pcon;
	struct mem_msg_block_t *msg = mem_msg_get_block();
	pcon = (onconnect_t *)msg->buf;
	pcon->con_handle = con_handle;
	memcpy(pcon->dev_addr, prsp->dst_dev_addr, sizeof(bdaddr));
	msg_post(pmsg_report, radio_connection_complete_event, msg);
	
  }

  return TRUE;
}



static uint8_t __do_connecting(ActionPacket* p)
{
  dis_con_count = 0;
  
  
  if(CONNECTED == con_sta){
    uint64_t cur_time;
    uint64_t STU_value = TIMER_GetAnchorPoint(&cur_time);
	next_wakeup_time = STU_value + (con_handle) * RADIO_TIME_UNIT;
	txPacket.WakeupTime = next_wakeup_time; 
	RADIO_SetReservedArea(&txPacket);

	return TRUE; 
  }

	return TRUE;
}

static uint8_t __do_lost_connection_packet(ActionPacket* p)
{
  uint8_t ret = TRUE;
  dis_con_count++;

  //COMPrintf("lost one packet\r\n");
  #if 1
  static uint32_t lost_msg_count = 0;
  lost_msg_count++;
  //if(lost_msg_count %3 == 0)
  {
    struct mem_msg_block_t *msg = mem_msg_get_block();
    msg_post(pmsg_report, radio_lost_packet, msg);
  }
  #endif
   
  if(dis_con_count > DEFUALT_CONNECTION_TIMEOUT){
  	__do_scan();
	ret = FALSE;
  }else{
    next_wakeup_time +=  (con_handle)*RADIO_TIME_UNIT;
    txPacket.WakeupTime = next_wakeup_time; 
    RADIO_SetReservedArea(&txPacket);
  }
  return ret;
}

static uint8_t rxConditionRoutine(ActionPacket* p)
{
  uint8_t ret = TRUE;

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
	/* received a packet */
	if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
		ret = __do_connecting(p);
	}
	else{
		ret = __do_lost_connection_packet(p);
	}
  }
  return ret;
}

static void report_rx_msg_to_app(ActionPacket* p,  ActionPacket* next)
{
  if((CONNECTED == con_sta) && (next == &txPacket)){
	struct mem_trans_block_t *prx_block_bac = mem_trans_get_block();
	if(prx_block_bac != NULL){
		uint16_t ret = msg_post(pmsg_report, radio_rx_event, prx_block);
		if(ret == OS_ERR_NONE){
			prx_block = prx_block_bac;
			rxPacket.data = prx_block->buf;
		}
		else{
            mem_trans_return_block(prx_block_bac);
		}
	}
  }
}

static void updata_connection_sta_and_data(ActionPacket* p,  ActionPacket* next)
{
	if((CONNECTING == con_sta) && (next == &txPacket)){
	  con_sta = CONNECTED;
	  
	}
	else if(next == &scanPacket){
	  con_sta = DISCONNECTED;
	  ptx_block = ptx_empty_block;
	  txPacket.data = ptx_block->buf;
	  //
	  ondisconnect_t *pdis;
	  struct mem_msg_block_t *msg = mem_msg_get_block();
	  if(msg != NULL){
		  pdis = (ondisconnect_t *)msg->buf;
		  pdis->con_handle = con_handle;  
		  //memcpy(pdis->dev_addr, peer_bdaddr, sizeof(peer_bdaddr));
		  msg_post(pmsg_report, radio_disconnection_complete_event, msg);
	  }
	}
	else if(next == &txPacket){
	  pscmd = (scmd_t*)(&ptx_block->buf[2]);
	  pscmd->head = shead;
	  ptx_block->buf[1] = sizeof(scmd_t);
	}

}
static uint8_t rxDataRoutine(ActionPacket* p,  ActionPacket* next)
{
  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
	/* received a packet */
	if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
		if((p->data[1] > (sizeof(mcmd_t)-USER_PACKET_SIZE)) && (con_sta == CONNECTED)){
		  mcmd_t* pmcmd = (mcmd_t*)(&p->data[2]);
		  uint16_t mNextTransmitSeqNum = pmcmd->head.transmitSeqNum +1;
		  //COMPrintf("mNextTransmitSeqNum:%d \r\n", mNextTransmitSeqNum);
		  // check master mNextTransmitSeqNum
		  if(mNextTransmitSeqNum != shead.mNextTransmitSeqNum){
		    shead.mNextTransmitSeqNum = mNextTransmitSeqNum;
			//COMPrintf_hexdump(pmcmd->buf,10);
			if(pmcmd->buf_len >0)
			  report_rx_msg_to_app(p, next);
		  }
          uint8_t byte_pos = (con_handle-1)/8;
          uint8_t byte_bit = (con_handle-1)%8;
		  // peer device has recived last sent packet.
		  if( ((pmcmd->head.nesn[byte_pos] & (1<<byte_bit)) == ((shead.sTransmitSeqNum+1)%2)) &&
		  	  (ptx_block != ptx_empty_block)){
			ptx_block = ptx_empty_block;
			txPacket.data = ptx_block->buf;
			// report this massge to app
			msg_post(pmsg_ctl_rsp, radio_tx_complete_event, cur_txmsg.param);
		  }
		  
		}
	}
	else{
		
	}
  }
  
  updata_connection_sta_and_data(p,next);

  
  return TRUE;
}


static uint8_t syncConditionRoutine(ActionPacket* p)
{
  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
    /* received a packet */
    if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
	  uint64_t cur_time;
	  uint64_t STU_value = TIMER_GetAnchorPoint(&cur_time);
	  next_wakeup_time = STU_value + 3*RADIO_TIME_UNIT;
	  rxPacket.WakeupTime = next_wakeup_time - NODES_RX_OFFSET; 
	  RADIO_SetReservedArea(&rxPacket);	
	  update_con_chan();
	  access_address = DEF_CON_ACCESS_ADDRESS;
  	  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
  	}else{
      next_wakeup_time += 3*RADIO_TIME_UNIT;
	  rxPacket.WakeupTime = next_wakeup_time - NODES_RX_OFFSET; 
	  RADIO_SetReservedArea(&rxPacket);	
	  update_con_chan();
	  access_address = DEF_CON_ACCESS_ADDRESS;
  	  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
	}
  }

  
  return TRUE;
  
}


static uint8_t syncDataRoutine(ActionPacket* p,  ActionPacket* next)
{

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
	/* received a packet */
	if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
      adv_packet_t *padv = (adv_packet_t*)(&p->data[2]);
      static uint16_t cmd_index = 0;
	  if(cmd_index != padv->sync.index){
	  	cmd_index = padv->sync.index;
	  	struct mem_trans_block_t *psync_block_bac = mem_trans_get_block();
		if(psync_block_bac != NULL){
          msg_post(pmsg_report, radio_adv_report, psync_block);
		  psync_block = psync_block_bac;
		}
	  }
	}
  }
  // 
  if(ptx_block == ptx_empty_block)
  	ctl_msg_schedule(pmsg_ctl);

  return TRUE;
}



static uint8_t txConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  (CONNECTION_INTERVAL - con_handle - 3) * RADIO_TIME_UNIT;
  syncPacket.WakeupTime = next_wakeup_time - NODES_RX_OFFSET; 
  RADIO_SetReservedArea(&syncPacket);
  cur_event_num++;
  select_sync_chan();
  access_address = ADV_ACCESS_ADDRESS;
  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
  RADIO_SetGlobalReceiveTimeout(DEFUALT_RECEIVETIMEOUT);
  return TRUE; 
}

/**
  * @brief  Data routine.
  * @param  ActionPacket: current
  * @param  ActionPacket: next
  * @retval TRUE
  */
static uint8_t dataRoutine(ActionPacket* p,  ActionPacket* next)
{

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
	/* received a packet */
	if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
	}
  }

  return TRUE;
}



static uint8_t txDataRoutine(ActionPacket* p,  ActionPacket* next)
{


  return TRUE;
}



static void initDumbPacket(void)
{
  next_wakeup_time = HAL_VTIMER_GetCurrentSysTime();
  /* Build Action Packet */
  dumbPacket.StateMachineNo = STATE_MACHINE_0;
  dumbPacket.ActionTag = PLL_TRIG |TIMER_WAKEUP | TIMESTAMP_POSITION;
  // 
  next_wakeup_time += RADIO_TIME_UNIT;
  dumbPacket.WakeupTime = next_wakeup_time;				 
  dumbPacket.MaxReceiveLength = 0;					/* Not applied for TX */
  dumbPacket.data = tx_data;							/* Data to send */
  dumbPacket.next_true = &scanPacket;				/* Pointer to the next Action Packet*/
  dumbPacket.next_false = &scanPacket;				/* Null */	 
  dumbPacket.condRoutine = dumbConditionRoutine;		  /* Condition routine */
  dumbPacket.dataRoutine = dataRoutine;				/* Data routine */
}

static void set_default_actionPacket(void)
{
  initDumbPacket();

  scanPacket.StateMachineNo = STATE_MACHINE_0;
  scanPacket.ActionTag = PLL_TRIG | TIMER_WAKEUP | TIMESTAMP_POSITION;			 
  scanPacket.MaxReceiveLength = MTU_SIZE;					/* Not applied for TX */
  scanPacket.data = psync_block->buf;							/* Data to send */
  scanPacket.next_true = &conReqPacket;				/* Pointer to the next Action Packet*/
  scanPacket.next_false = &scanPacket;				/* Null */	 
  scanPacket.condRoutine = scanConditionRoutine;		  /* Condition routine */
  scanPacket.dataRoutine = scanDataRoutine;				/* Data routine */

  conReqPacket.StateMachineNo = STATE_MACHINE_0;
  conReqPacket.ActionTag = PLL_TRIG | TXRX;
  conReqPacket.MaxReceiveLength = MTU_SIZE;					/* Not applied for TX */
  conReqPacket.data = con_req;							/* Data to send */
  conReqPacket.next_true = &conRspPacket;				/* Pointer to the next Action Packet*/
  conReqPacket.next_false = &conRspPacket;				/* Null */	 
  conReqPacket.condRoutine = conReqConditionRoutine;		  /* Condition routine */
  conReqPacket.dataRoutine = conReqDataRoutine;				/* Data routine */


  conRspPacket.StateMachineNo = STATE_MACHINE_0;
  conRspPacket.ActionTag = PLL_TRIG  | TIMESTAMP_POSITION;
  conRspPacket.MaxReceiveLength = MTU_SIZE;					/* Not applied for TX */
  conRspPacket.data = con_rsprx;							/* Data to send */
  conRspPacket.next_true = &rxPacket;				/* Pointer to the next Action Packet*/
  conRspPacket.next_false = &scanPacket;				/* Null */	 
  conRspPacket.condRoutine = conRspConditionRoutine;		  /* Condition routine */
  conRspPacket.dataRoutine = conRspDataRoutine;				/* Data routine */


  rxPacket.StateMachineNo = STATE_MACHINE_0;
  rxPacket.ActionTag = PLL_TRIG | TIMER_WAKEUP | TIMESTAMP_POSITION;			 
  rxPacket.MaxReceiveLength = MTU_SIZE;					/* Not applied for TX */
  rxPacket.data = prx_block->buf;							/* Data to send */
  rxPacket.next_true = &txPacket;				/* Pointer to the next Action Packet*/
  rxPacket.next_false = &scanPacket;				/* Null */	 
  rxPacket.condRoutine = rxConditionRoutine;		  /* Condition routine */
  rxPacket.dataRoutine = rxDataRoutine;				/* Data routine */

  txPacket.StateMachineNo = STATE_MACHINE_0;
  txPacket.ActionTag = TIMER_WAKEUP | TXRX ;
  txPacket.MaxReceiveLength = MTU_SIZE;					/* Not applied for TX */
  txPacket.data = ptx_block->buf;							/* Data to send */
  txPacket.next_true = &syncPacket;				/* Pointer to the next Action Packet*/
  txPacket.next_false = &syncPacket;				/* Null */	 
  txPacket.condRoutine = txConditionRoutine;		  /* Condition routine */
  txPacket.dataRoutine = txDataRoutine;				/* Data routine */
  pscmd = (scmd_t*)(&ptx_block->buf[2]);
  pscmd->cmd = CMD_SLAVE_TX;


  syncPacket.StateMachineNo = STATE_MACHINE_0;
  syncPacket.ActionTag = PLL_TRIG | TIMER_WAKEUP | TIMESTAMP_POSITION;
  syncPacket.MaxReceiveLength = MTU_SIZE;					/* Not applied for TX */
  syncPacket.data = psync_block->buf;							/* Data to send */
  syncPacket.next_true = &rxPacket;				/* Pointer to the next Action Packet*/
  syncPacket.next_false = &rxPacket;				/* Null */	 
  syncPacket.condRoutine = syncConditionRoutine;		  /* Condition routine */
  syncPacket.dataRoutine = syncDataRoutine;				/* Data routine */

  RADIO_SetReservedArea(&dumbPacket);
  RADIO_SetReservedArea(&scanPacket);;
  RADIO_SetReservedArea(&conReqPacket);
  RADIO_SetReservedArea(&conRspPacket);
  RADIO_SetReservedArea(&rxPacket);
  RADIO_SetReservedArea(&txPacket);
  RADIO_SetReservedArea(&syncPacket);
  
}

static void enable_BLE_RXTX_SEQ_IRQ(void)
{
  LL_APB0_EnableClock(LL_APB0_PERIPH_SYSCFG);
  
  LL_SYSCFG_BLERXTX_SetTrigger(LL_SYSCFG_BLERXTX_TRIGGER_BOTH_EDGE, LL_SYSCFG_BLE_TX_EVENT);
  LL_SYSCFG_BLERXTX_SetTrigger(LL_SYSCFG_BLERXTX_TRIGGER_BOTH_EDGE, LL_SYSCFG_BLE_RX_EVENT);
  
  LL_SYSCFG_BLERXTX_SetType(LL_SYSCFG_BLERXTX_DET_TYPE_EDGE, LL_SYSCFG_BLE_TX_EVENT);
  LL_SYSCFG_BLERXTX_SetType(LL_SYSCFG_BLERXTX_DET_TYPE_EDGE, LL_SYSCFG_BLE_RX_EVENT);
  LL_SYSCFG_BLERXTX_EnableIT(LL_SYSCFG_BLE_TX_EVENT|LL_SYSCFG_BLE_RX_EVENT);
  NVIC_EnableIRQ(BLE_SEQ_IRQn);
}

void BLE_RXTX_SEQ_IRQHandler(void)
{
  if(LL_SYSCFG_BLERXTX_IsInterruptPending(LL_SYSCFG_BLE_TX_EVENT))
  {
    if(RRM->FSM_STATUS_DIG_OUT & RRM_FSM_STATUS_DIG_OUT_STATUS_4)
    {
      // Rising edge
      //COMPrintf("->");
      MASK_INTERRUPTS();
      if(radio_sta == STA_IDLE){
		radio_sta = STA_ACTIVE;
	  }
	  UNMASK_INTERRUPTS();
    }
    else
    {
      // Falling edge
      //COMPrintf("<-");
    }  
    // Do something
    LL_SYSCFG_BLERXTX_ClearInterrupt(LL_SYSCFG_BLE_TX_EVENT);
  }
  else if(LL_SYSCFG_BLERXTX_IsInterruptPending(LL_SYSCFG_BLE_RX_EVENT))
  {
    if(RRM->FSM_STATUS_DIG_OUT & RRM_FSM_STATUS_DIG_OUT_STATUS_4)
    {
      // Rising edge
    }
    else
    {
      // Falling edge
    }  
    // Do something
    LL_SYSCFG_BLERXTX_ClearInterrupt(LL_SYSCFG_BLE_RX_EVENT);
  }
}

static void app_chan_init(void)
{
  uint8_t map[5] = DEFUALT_CON_CHAN_MAP;
  ull_chan_map_set(map);
}

static void update_con_chan(void)
{
  uint8_t ch_map[5];
  uint8_t chan_count = ull_chan_map_get(ch_map);
  channel = lll_chan_sel_2(cur_event_num, 0x305F, ch_map,chan_count); 
  RADIO_SetChannel(STATE_MACHINE_0, channel, 0);
}


void ctl_init(queue_msg_t *pctl, queue_msg_t *pctlrsp,queue_msg_t *preport)
{
  // mem relative init
  pmsg_ctl = pctl;
  pmsg_ctl_rsp = pctlrsp;
  pmsg_report = preport;
  
  prx_block = mem_trans_get_block();
  psync_block = mem_trans_get_block();
  ptx_empty_block = mem_trans_get_block();
  ptx_block = ptx_empty_block;
  
  generate_static_mac_addr(bdaddr);
  COMPrintf("mac:");
  COMPrintf_hexdump(bdaddr,sizeof(bdaddr));

  app_chan_init();
  
  set_default_actionPacket();

  enable_BLE_RXTX_SEQ_IRQ();

  /* Channel map configuration */
  uint8_t map[5]= {0xFF,0xFF,0xFF,0xFF,0xFF};
  RADIO_SetChannelMap(STATE_MACHINE_0, &map[0]);

  RADIO_SetGlobalReceiveTimeout(DEFUALT_RECEIVETIMEOUT);
  RADIO_SetMaxReceivedLength(STATE_MACHINE_0, 200);

  RADIO_SetPhy(STATE_MACHINE_0, PHY_2M);
  RADIO_SetBackToBackTime(BACK_TO_BACK_TIME);
  
  /* Setting of channel and the channel increment*/
  RADIO_SetChannel(STATE_MACHINE_0, channel, 0);

  /* Sets of the NetworkID and the CRC.*/
  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
  
  /* Configures the transmit power level */
  RADIO_SetTxPower(MAX_OUTPUT_RF_POWER);  
  LL_PWR_SetSMPSOutputLevel(LL_PWR_SMPS_OUTLVL_1V95);

  radio_sta = STA_IDLE;
  RADIO_MakeActionPacketPending(&dumbPacket);
}


static uint8_t ctl_msg_schedule(queue_msg_t *msg_head)
{
	//memset(&msg, 0, sizeof(msg));
	
	uint16_t ret = msg_post_accept(msg_head, &cur_txmsg);
	if (OS_ERR_NONE == ret){
		if (cur_txmsg.handler != NULL){
			if(con_sta == CONNECTED){
			  cur_txmsg.handler(cur_txmsg.param);
			}
			else{
			  // mem recycle
			  if(mem_is_msg_range_addr(cur_txmsg.param)){
				mem_msg_return_block((struct mem_msg_block_t *)cur_txmsg.param);
			  }else if(mem_is_trans_range_addr(cur_txmsg.param)){
	             mem_trans_return_one_tx_block((struct mem_trans_block_t *)cur_txmsg.param);
			  }
			  memset(&cur_txmsg, 0, sizeof(cur_txmsg));
			}

		}
	}

  return ret;
}


void radio_tx(void* param)
{
  COMPrintf("radio_tx \r\n");
  struct mem_trans_block_t *pmsg = (struct mem_trans_block_t*)param;  
  ptx_block = pmsg;
  txPacket.data = ptx_block->buf;
  shead.sTransmitSeqNum++;
  ptx_block->buf[1] = sizeof(scmd_t);
  scmd_t *pscmd = (scmd_t*)(&ptx_block->buf[2]);
  pscmd->head = shead;
  
}





