// mem_mng.c
#include "mem_mng.h"
#include "module.h"
#include "system_BlueNRG_LP.h"
#include "cassert.h"

#define disable_irqs() __disable_irq(); 
#define enable_irqs() __enable_irq();


static struct mem_msg_block_t _msg_memory[MSG_FIFO_NUM];
static struct dl_list s_msg_unuse;
static struct dl_list s_msg_list;

static struct mem_trans_block_t _trans_memory[TRANST_FIFO_NUM];
static struct dl_list s_trans_unuse;
static struct dl_list s_tx_list;
static uint32_t tx_list_len = 0;
static struct dl_list s_rx_list;


void mem_msg_list_init(void)
{
	dl_list_init(&s_msg_list);
	dl_list_init(&s_msg_unuse);

	for (int i = 0; i < ARRAY_LEN(_msg_memory); ++i) {
      dl_list_add(&s_msg_unuse, &_msg_memory[i].node);
    }
}


struct mem_msg_block_t* mem_msg_get_block(void)
{
  struct mem_msg_block_t *item;
  disable_irqs();
  item = dl_list_first(&s_msg_unuse, struct mem_msg_block_t, node);
  if(item != NULL){
    dl_list_del(&item->node);
	dl_list_add(&s_msg_list, &item->node);
	//COMPrintf("msg_get:%x \r\n", item);
  }
  enable_irqs();
  return item;
}


void mem_msg_return_block(struct mem_msg_block_t * item)
{
  if(item != NULL){
  	disable_irqs();
    dl_list_del(&item->node);
	dl_list_add(&s_msg_unuse, &item->node);
	//COMPrintf("return:%x \r\n", item);
	enable_irqs();
  }
}

uint8_t mem_is_msg_range_addr(struct mem_msg_block_t * item)
{
  if( (item != NULL) 
  && (item <= &_msg_memory[MSG_FIFO_NUM-1]) 
  && (item >= &_msg_memory[0])){
    return TRUE;
  }else{
    return FALSE;
  }
}

///////-----------------------------------------------/////////


void mem_trans_list_init(void)
{
	dl_list_init(&s_trans_unuse);
	dl_list_init(&s_tx_list);
	dl_list_init(&s_rx_list);

	for (int i = 0; i < ARRAY_LEN(_trans_memory); ++i) {
      dl_list_add(&s_trans_unuse, &_trans_memory[i].node);
    }
}


struct mem_trans_block_t* mem_trans_tx_get_block(void)
{
  struct mem_trans_block_t *item = NULL;
  if(tx_list_len >= TRANST_FIFO_NUM/2)
  	return item;
  disable_irqs();
  item = dl_list_first(&s_trans_unuse, struct mem_trans_block_t, node);
  if(item != NULL){
    dl_list_del(&item->node);
	tx_list_len++;
	dl_list_add(&s_tx_list, &item->node);
  }
  enable_irqs();
  return item;
}

struct mem_trans_block_t* mem_trans_get_block(void)
{
  struct mem_trans_block_t *item;
  disable_irqs();
  item = dl_list_first(&s_trans_unuse, struct mem_trans_block_t, node);
  if(item != NULL){
    dl_list_del(&item->node);
	dl_list_add(&s_rx_list, &item->node);
  }
  enable_irqs();
  return item;
}

// 
void mem_trans_return_all_tx_block(void)
{
  struct mem_trans_block_t *item;
  while(1){
	  disable_irqs();
	  item = dl_list_first(&s_tx_list, struct mem_trans_block_t, node);
	  if(item != NULL){
	    dl_list_del(&item->node);
		if(tx_list_len)
			tx_list_len--;
		dl_list_add(&s_trans_unuse, &item->node);
	  }
	  else{
	  	enable_irqs();
		break;
	  }
	  enable_irqs();
  }
}


struct mem_trans_block_t * mem_trans_return_one_tx_block(struct mem_trans_block_t *item)
{

  disable_irqs();
  if(item != NULL){
    dl_list_del(&item->node);
	dl_list_add(&s_trans_unuse, &item->node);
	if(tx_list_len)
	  tx_list_len--;
  }
  enable_irqs();
  return item;
}


void mem_trans_return_block(struct mem_trans_block_t * item)
{
  if(item != NULL){
  	disable_irqs();
    dl_list_del(&item->node);
	dl_list_add(&s_trans_unuse, &item->node);
	enable_irqs();
  }
}


uint8_t mem_is_trans_range_addr(struct mem_trans_block_t * item)
{
  if( (item != NULL) && \
      (item <= &_trans_memory[TRANST_FIFO_NUM-1]) && \
      (item >= &_trans_memory[0])){
    return TRUE;
  }else{
    return FALSE;
  }
}





void mem_init(void)
{
  mem_msg_list_init();
  mem_trans_list_init();
}


