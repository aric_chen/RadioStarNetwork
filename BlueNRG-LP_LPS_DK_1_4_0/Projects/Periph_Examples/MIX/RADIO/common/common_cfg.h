// common_cfg.h

#ifndef __COMMON_CFG_H__
#define __COMMON_CFG_H__




// 
#define NODE_NUMBER    		 (60u)
#define UINT64_NODE_ARR      (NODE_NUMBER/64 + (NODE_NUMBER%64 == 0 ? 0:1))
#define UINT8_NODE_ARR		 (NODE_NUMBER/8 + (NODE_NUMBER%8 == 0 ? 0:1))
// 
#define CONNECTION_INTERVAL  (NODE_NUMBER + 4)

// time slot
#define RADIO_TIME_UNIT  (409u)  // 1ms  ////2.44140625 us 

//#define RADIO_ACTIVE_MUTEX

#ifdef RADIO_ACTIVE_MUTEX
  #define RADIO_SKIP_PERIOD      (10)  // 
  #define RADIO_SMALL_SKIP_SLOT  (1)   //
#endif

#define ADV_ACCESS_ADDRESS      (uint32_t)(0x8E89BED6)

#define DEF_CON_ACCESS_ADDRESS  (uint32_t)(0x0E89BED6)

#define NODES_RX_OFFSET	(17)  //2.44140625 us 

// should be more than CONNECTION_INTERVAL
#define DEFUALT_SCAN_INTERVAL (1000000)	// unis:us




#define DEFUALT_SCAN_INTERSPACE (100u)   //unit:ticks


#define DEFUALT_RECEIVETIMEOUT  (410u)   // unis:us


#define DEFUALT_CONNECTION_TIMEOUT	(20)  // TIMES

#define DEFUALT_SCAN_CHAN_MAP       {37,38,39}

#define DEFUALT_CON_CHAN_MAP        {0x00, 0x06, 0xE0, 0x00, 0x1C}



#define MAX_RECEIVE_LENGTH			(150u)

#define MTU_SIZE					(100u)
#define USER_PACKET_SIZE			(64u)
#define PDU_HEAD_RSEV_SIZE			(20u)

//#define BACK_TO_BACK_TIME           (150)



#endif
