// ull_chan.h

#ifndef __ULL_CHAN_H__

#include <stdint.h>
#include <string.h>


int ull_chan_reset(void);

uint8_t ull_chan_map_get(uint8_t *const chan_map);

void ull_chan_map_set(uint8_t const *const chan_map);

uint8_t util_ones_count_get(uint8_t *octets, uint8_t octets_len);




#endif


