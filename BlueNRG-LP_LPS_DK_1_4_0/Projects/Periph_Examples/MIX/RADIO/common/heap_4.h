#ifndef __HEAP_4_H__
#define __HEAP_4_H__

#include <stdlib.h>
#include <stdint.h>

void *pvPortMalloc( size_t xWantedSize );




void vPortFree( void *pv );


#endif

