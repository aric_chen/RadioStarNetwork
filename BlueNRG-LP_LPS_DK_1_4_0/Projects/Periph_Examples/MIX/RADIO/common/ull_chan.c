// ull_chan.c

#include "ull_chan.h"

/* Initial channel map indicating Used and Unused data channels.
 * The HCI LE Set Host Channel Classification command allows the Host to
 * specify a channel classification for the data, secondary advertising,
 * periodic, and isochronous physical channels based on its local information.
 */

static uint8_t map[5] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
static uint8_t count = 40U; 
/*
// 
static const uint8_t chan_remap_table [40] =
{

  37,   0,   1,   2,   3,   4,   5,   6,   
  7,    8,   9,   10,  38,  11,  12,  13,
  14,   15,  16,  17,  18,  19,  20,  21,
  22,   23,  24,  25,  26,  27,  28,  29,
  30,  31,   32,  33,  34,  35,  36,  39,
//
  1,    2,   3,   4,   5,   6,   7,   8,
  9,    10,  11,  13,  14,  15,  16,  17,  
  18,   19,   20,  21,  22,  23,  24,  25,
  26,   27,   28,  29,  30,  31,  32,  33,
  34,   35,   36,  37,  38,   0,   12,  39,  
};
*/
int ull_chan_reset(void)
{
	/* initialise connection channel map */
	map[0] = 0xFF;
	map[1] = 0xFF;
	map[2] = 0xFF;
	map[3] = 0xFF;
	map[4] = 0xFF;
	
	count = 40U;

	return 0;
}

uint8_t ull_chan_map_get(uint8_t *const chan_map)
{
	memcpy(chan_map, map, sizeof(map));

	return count;
}

uint8_t util_ones_count_get(uint8_t *octets, uint8_t octets_len)
{
	uint8_t one_count = 0U;

	while (octets_len--) {
		uint8_t bite;

		bite = *octets;
		while (bite) {
			bite &= (bite - 1);
			one_count++;
		}
		octets++;
	}

	return one_count;
}

void ull_chan_map_set(uint8_t const *const chan_map)
{
	memcpy(map, chan_map, sizeof(map));
	count = util_ones_count_get(map, sizeof(map));
}
