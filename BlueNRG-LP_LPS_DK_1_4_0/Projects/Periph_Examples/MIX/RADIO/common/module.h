/******************************************************************************
 * @brief   
 *
 * Copyright (c) 2017~2020, <                 >
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs: 
 * Date           Author     Notes 
 ******************************************************************************/

#ifndef __MODULE_H_
#define __MODULE_H_

#include "comdef.h"
//#include "compiler.h"
#include <stdbool.h>
#include <stdint.h>

/*module item*/
typedef struct {
    const char *name;               //
    void (*init)(void);             //
}init_item_t;

/* task item*/
typedef struct {
    const char *name;               //  
    void (*handle)(void);           //
    unsigned long long interval;    //
    unsigned long long *timer;      //
}task_item_t;

#define __module_initialize(name,func,level)           \
    USER_USED ANONY_TYPE(const init_item_t, init_tbl_##func)\
    USER_SECTION("init.item."level) = {name,func}

/*
 * @brief     register a stack  
 * @param[in]   name    - task name
 * @param[in]   handle  - process callback(void func(void){...})
  * @param[in]  interval- The cycle of processing the callback.
 */
#define task_register(name, handle,interval)                \
    static uint64_t __task_timer_##handle;              \
    USER_USED ANONY_TYPE(const task_item_t, task_item_##handle)  \
    USER_SECTION("task.item.1") =                                \
    {name,handle, interval, &__task_timer_##handle}

/*
 * @brief  priority: system > driver > module  
 * @param[in]   name    - system, driver, module, name
 * @param[in]   func    - init callback(void func(void){...})
 */
#define system_init(name,func)  __module_initialize(name,func,"1")
#define driver_init(name,func)  __module_initialize(name,func,"2")
#define module_init(name,func)  __module_initialize(name,func,"3")

//void systick_increase(unsigned int ms);
uint64_t get_tick(void);
bool is_timeout(uint64_t start, uint64_t timeout);
void module_task_init(void);
void module_task_process(void);
    
#endif
