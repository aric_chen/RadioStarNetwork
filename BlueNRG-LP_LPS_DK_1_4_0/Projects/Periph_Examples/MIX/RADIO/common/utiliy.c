#include "utility.h"
#include "cassert.h"
#include "module.h"
#include "common_cfg.h"




static uint8_t table[256] = 
{ 
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, 
}; 
 
uint8_t count_bits(uint32_t value)
{
    uint8_t count = 0;
    count  =  table[ value & 0xff ] +
              table[( value >>8 ) & 0xff ] +
              table[( value >>16 ) & 0xff ] +
              table[( value >>24 ) & 0xff ] ;
    return count ;
}

uint8_t count_bits_uint64(uint64_t value)
{
	uint8_t count = 0;
	 
	count  =  table[ value & 0xff ] +
			  table[( value >>8 ) & 0xff ] +
			  table[( value >>16 ) & 0xff ] +
			  table[( value >>24 ) & 0xff ] + 
			  table[( value >>32 ) & 0xff ] + 
			  table[( value >>40 ) & 0xff ] + 
			  table[( value >>48 ) & 0xff ] + 
			  table[( value >>56 ) & 0xff ] ; 
	
	return count ;
}

static uint8_t high_table[256] = {
 0, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 3, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 2, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 3, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 1, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 3, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 2, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 3, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
 4, 8, 7, 8, 6, 8, 7, 8, 5, 8, 7, 8, 6, 8, 7, 8,
};

static uint8_t low_table[256] = {
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 6,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 7,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 6,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 8,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 6,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 7,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 6,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5,
	1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 0,
};

uint8_t get_high_bit(uint8_t data)
{
  return high_table[data];
}

uint8_t get_low_bit(uint8_t data)
{
  return low_table[data];
}


uint16_t get_low_bit_arr(uint8_t* dev_csta, uint8_t length)
{
  uint16_t pos = 0;
  for(int i=0;i<length;i++){
     uint8_t ret = low_table[dev_csta[i]];
	 if(ret > 0){
	 	pos = ret + i*8;
        break;
	 }
  }

  if(pos > NODE_NUMBER)
  	pos = 0;

  return pos;
}

void set_bit(uint8_t* arr, uint8_t length, uint16_t pos)
{
  if(pos < length*8){
	uint8_t byte_pos = (pos)/8 ;
	uint8_t byte_bit = (pos)%8;
	arr[byte_pos] |= (1<<byte_bit);
  }
}

void clear_bit(uint8_t* arr, uint8_t length, uint16_t pos)
{
  if(pos < length*8){
	uint8_t byte_pos = (pos)/8 ;
	uint8_t byte_bit = (pos)%8;
	arr[byte_pos] &= (~(1<<byte_bit));
  }
}

BOOL get_bit(uint8_t* arr, uint8_t length, uint16_t pos)
{

	uint8_t byte_pos = (pos)/8 ;
	uint8_t byte_bit = (pos)%8;
	return ((arr[byte_pos] & (1<<byte_bit)) >0 );

}




