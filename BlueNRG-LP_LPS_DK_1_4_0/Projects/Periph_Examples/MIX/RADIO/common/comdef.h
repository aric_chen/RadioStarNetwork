/******************************************************************************
 * @brief    ͨ�ú궨��
 *
 * Copyright (c) 2018~2020, <                 >
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs: 
 * Date           Author       Notes 
 * 2018-02-01                Initial version. 
 ******************************************************************************/
 
#ifndef _COM_DEF_H_
#define _COM_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

/*�������Ͷ��� -----------------------------------------------------------*/
#define  ANONY_CONN(type, var, line)  type  var##line
#define  ANONY_DEF(type,prefix,line)  ANONY_CONN(type, prefix, line)
#define  ANONY_TYPE(type,prefix)      ANONY_DEF(type, prefix, __LINE__)

/**
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:	the pointer to the member.
 * @type:	the type of the container struct this is embedded in.
 * @member:	the name of the member within the struct.
 *
 */
#define container_of(ptr, type, member) ( \
	(type *)( (char *)(ptr) - offsetof(type,member) ))


#if defined(__CC_ARM) || defined(__GNUC__) /* ARM,GCC*/
    #define USER_SECTION(x)            __attribute__((section(x)))
    #define USER_UNUSED                      __attribute__((unused))
    #define USER_USED                        __attribute__((used))
    #define USER_ALIGN(n)                    __attribute__((aligned(n)))

    #define USER_WEAK                        __attribute__((weak))
#elif defined (__ICCARM__)              /*IAR */
    #define USER_SECTION(x)                  	@ x
    #define USER_UNUSED                      
    #define USER_USED                        __root
    #define USER_WEAK                        __weak
#else
    #error "Current tool chain haven't supported yet!"
#endif

#ifdef __cplusplus
}
#endif

#endif
