#ifndef __CASSERT_H__
#define __CASSERT_H__


#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "rf_driver_ll_bus.h"
#include "rf_driver_ll_cortex.h"
#include "rf_driver_ll_rcc.h"
#include "rf_driver_ll_system.h"
#include "rf_driver_ll_utils.h"
#include "rf_driver_ll_gpio.h"
#include "rf_driver_ll_lpuart.h"

#if defined(CONFIG_DEVICE_BLUENRG_LP) || defined(CONFIG_DEVICE_BLUENRG_LPS)
#include "bluenrg_lpx.h"
#endif




void COMPrintf_init(void);

int COMPrintf(const char* fmt, ...);

int COMPrintf_hexdump(uint8_t *parr,uint8_t len);



#endif


