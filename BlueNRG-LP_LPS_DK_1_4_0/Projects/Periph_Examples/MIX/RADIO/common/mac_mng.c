#include "mac_mng.h"
#include "sha256.h"
#include "rf_driver_ll_utils.h"
#include "rf_driver_ll_rcc.h"
#include "rf_driver_ll_bus.h"
#include "rf_driver_ll_system.h"
#include "rf_driver_ll_exti.h"
#include "rf_driver_ll_cortex.h"
#include "rf_driver_ll_utils.h"
#include "rf_driver_ll_rng.h"


#define    NB_OF_GENERATED_RANDOM_NUMBERS      4       /* Nb of Random numbers generated after eash User button press */

/* Array used for storing generated Random 32bit Numbers */
uint16_t aRandom16bit[NB_OF_GENERATED_RANDOM_NUMBERS];



static void MX_RNG_Init(void)
{
  /* Peripheral clock enable */
  LL_AHB_EnableClock(LL_AHB_PERIPH_RNG);
  
  LL_RNG_Enable(RNG);
}



/**
* @brief  This function performs several random numbers generation.
* @note   Generated random numbers are stored in global variable array, so that
*         generated values could be observed by user by watching variable content
*         in specific debugger window
* @param  None
* @retval None
*/
void RandomNumbersGeneration(void)
{
  register uint8_t index = 0;
  
  /* Initialize random numbers generation */
  LL_RNG_Enable(RNG);
  
  /* Generate Random 16bit Numbers */
  for(index = 0; index < NB_OF_GENERATED_RANDOM_NUMBERS; index++)
  {

    
    /* Wait for DRDY flag to be raised */
    while (!LL_RNG_IsActiveFlag_RNGRDY(RNG))
    {

    }
    
    /* Check if error occurs */
    if (  LL_RNG_IsActiveFlag_FAULT(RNG)  )
    {
      /* Clock or Seed Error detected. Set LED to blinking mode (Error type)*/
    }
    
    /* Otherwise, no error detected : Value of generated random number could be retrieved
    and stored in dedicated array */
    aRandom16bit[index] = LL_RNG_ReadRandData16(RNG);
  }
  
  /* Stop random numbers generation */
  LL_RNG_Disable(RNG);

}


// 
void generate_static_mac_addr(uint8_t bdaddr[6])
{
	// get UID
	BYTE text1[8];
	uint32_t word0 = LL_GetUID_Word0();
	uint32_t word1 = LL_GetUID_Word1();
        if(word0 == 0xffffffff && word1 == 0xffffffff){
          MX_RNG_Init();
          RandomNumbersGeneration();
          memcpy(text1,&aRandom16bit,sizeof(text1));
        }
        else{
          memcpy(text1,&word0,sizeof(word0));
	  memcpy(&text1[4],&word1,sizeof(word1));
        }
        
        
	
	BYTE buf[SHA256_BLOCK_SIZE];
	SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, text1, sizeof(text1));
	sha256_final(&ctx, buf);
	memcpy(bdaddr,buf,6);
}
