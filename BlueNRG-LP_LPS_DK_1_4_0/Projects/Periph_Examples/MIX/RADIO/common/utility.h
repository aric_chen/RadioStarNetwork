// utility.h
#ifndef __UTILITY_H__
#define __UTILITY_H__


#include <stdint.h>

#include "system_util.h"


uint32_t fls8(uint8_t x);


uint32_t fls(uint32_t x);






uint32_t fls64(uint64_t x);


uint8_t count_bits(uint32_t value);

uint8_t count_bits_uint64(uint64_t value);



uint8_t get_low_bit(uint8_t data);

uint16_t get_low_bit_arr(uint8_t* dev_csta, uint8_t length);



void set_bit(uint8_t* arr, uint8_t length, uint16_t pos);
void clear_bit(uint8_t* arr, uint8_t length, uint16_t pos);
BOOL get_bit(uint8_t* arr, uint8_t length, uint16_t pos);









#endif

