// ll_pdu.h
#ifndef __LL_PDU_H__
#define __LL_PDU_H__
#include <stdint.h>
#include "common_cfg.h"

/*
协议格式：  协议版本 1byte + LEN（总包长度）[1 byte] + CMD[1 byte] + TLV格式数据包[N]
协议格式：  协议版本 1byte + LEN（总包长度）[1 byte] + head + TLV格式数据包[N]
*/


enum {
  // adv channle 
  CMD_ROLL_ALL,		// 
  CMD_ROLL_GROUP,
  CMD_ROLL_ONE,
  CMD_CONNECTION_REQ,
  CMD_CONNECTION_RSP,

  // data channel
  CMD_MASTER_TX_ALL,

  CMD_SLAVE_TX,

  CMD_TX_CTL_NO_ACK,
  
};


enum{
  TLV_MAC = 0x00,   	// 06: 80:EA:CA:00:00:00
  TLV_CUR_EVEN,         // 02: value
  TLV_ADV_INDEX,		// 01: value 
  TLV_GROUP,  			// 01: value 
  TLV_CON_EVENT_NUM,    // 01: value 
  TLV_CON_HANDLE, 		// 01: value 
  TLV_TIME,				// 08: value 
  TLV_HEADER_SLAVE,		// NESN(1 bit) + SN (1 bit)
  TLV_HEADER_MASTER,	// （NESN(60 bit) + SN (60 bit)）

};

#pragma pack(1)

/*
CMD_ROLL_ALL
*/

typedef struct{
    uint32_t dis_con_count;
	uint8_t dev_addr[6];
	uint16_t mNextTransmitSeqNum;
}dev_t;


typedef struct{
  uint16_t index;
  uint8_t cmd;
  uint8_t buf_len;
  uint8_t buf[USER_PACKET_SIZE];
}adv_sync_t;

// CMD_ROLL_ALL
typedef struct{
	uint8_t cmd;
	uint8_t dev_addr[6];
	uint16_t cur_event_num;
	//uint64_t cur_time;
	uint8_t discoverable;
	uint8_t node_sta[UINT8_NODE_ARR];
	adv_sync_t sync;
}adv_packet_t;

// CMD_CONNECTION_REQ
typedef struct{
    uint8_t cmd;
    uint8_t dev_addr[6];
}con_req_t;

// CMD_CONNECTION_RSP
typedef struct{
    uint8_t cmd;
    uint8_t src_dev_addr[6];
	uint8_t dst_dev_addr[6];
	uint16_t con_handle;
	//uint32_t access_addr;
}con_rsp_t;



typedef struct{
  uint16_t transmitSeqNum;
  uint8_t nesn[UINT8_NODE_ARR];
}mcmd_head_t;

typedef struct{
 uint16_t mNextTransmitSeqNum;
 uint16_t sTransmitSeqNum;
}scmd_head_t;



// CMD_MASTER_TX_ALL
typedef struct{
    uint8_t cmd;
	mcmd_head_t head;
	uint8_t buf_len;
	uint8_t buf[USER_PACKET_SIZE];
}mcmd_t;


// CMD_SLAVE_TX
typedef struct{
    uint8_t cmd;
	scmd_head_t head;
	uint8_t buf_len;
	uint8_t buf[USER_PACKET_SIZE];
}scmd_t;



typedef struct{
    uint16_t con_handle;
	uint8_t dev_addr[6];
}onconnect_t;


typedef struct{
    uint16_t con_handle;
	uint8_t dev_addr[6];
}ondisconnect_t;










#pragma pack()


#endif


