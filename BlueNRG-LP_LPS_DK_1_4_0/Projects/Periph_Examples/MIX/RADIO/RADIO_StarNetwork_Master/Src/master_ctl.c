#include "usr_cfg.h"
#include "master_ctl.h"
#include "rf_driver_ll_radio_2g4.h"
#include "rf_driver_hal_vtimer.h"
#include "cassert.h"
#include "mac_mng.h"
#include "ull_chan.h"
#include "lll_chan.h"
#include "ll_pdu.h"
#include "qmsg.h"
#include "mem_mng.h"
#include "radio_event.h"
#include "utility.h"


#define MASK_INTERRUPTS() __disable_irq(); 
#define UNMASK_INTERRUPTS() __enable_irq();
      

static ActionPacket dumbPacket;
// advertise and send cmd
static ActionPacket advPacket;  
// wait for node connection 
static ActionPacket rxConPacket;
// response to create connection
static ActionPacket rspConPacket;

// cmd 
static ActionPacket cmdPacket;
// RX for nodes
static ActionPacket rxPacket[NODE_NUMBER];
static dev_t dev[NODE_NUMBER];


static uint32_t next_wakeup_time = 0;
static uint8_t channel = 16;   /* Start Channel */
static uint8_t adv_chan_index = 0;
static uint8_t scan_chan_map[3] = DEFUALT_SCAN_CHAN_MAP;

static adv_packet_t* padv;

static uint8_t radio_sta = STA_UNINIT;
static uint16_t cur_event_num = 0;
static uint32_t access_address = ADV_ACCESS_ADDRESS;
// local device address
static uint8_t bdaddr[6];
static uint16_t cur_node = 0;


static uint8_t dev_csta[UINT8_NODE_ARR];
static uint8_t csta_all_dis[UINT8_NODE_ARR] = {0};

static uint8_t dev_acked[UINT8_NODE_ARR];

static uint16_t cur_free_con_handle = 0;



static queue_msg_t *pmsg_ctl;
static queue_msg_t *pmsg_ctl_rsp;
static queue_msg_t *pmsg_report;

static struct mem_trans_block_t *prx_block[NODE_NUMBER];
static struct mem_trans_block_t *padv_block;
static struct mem_trans_block_t *prxConreq_block;
static struct mem_trans_block_t *pConrsp_block;
static struct mem_trans_block_t *pcmd_block;
static struct mem_trans_block_t *ptx_empty_block;

static msg_t cur_cmdmsg;
static mcmd_head_t mcmd_head;




static void update_con_chan(void);
static void set_adv_data(void);
static uint8_t ctl_msg_schedule(queue_msg_t *msg_head);
static uint8_t ctl_msg_schedule_and_clear(queue_msg_t *msg_head);



static uint8_t select_adv_chan(void)
{
    uint8_t index = cur_event_num % 3;
	RADIO_SetChannel(STATE_MACHINE_0, scan_chan_map[index], 0);
	return adv_chan_index;
}

static void set_adv_data(void)
{
	padv_block->buf[0] = 0;
	padv_block->buf[1] = sizeof(adv_packet_t);
	padv = (adv_packet_t*)(&padv_block->buf[2]);
	padv->cmd = CMD_ROLL_ALL;
	padv->cur_event_num = cur_event_num; 

    cur_free_con_handle = get_low_bit_arr(dev_csta, UINT8_NODE_ARR);
}


/*

*/
uint8_t read_next_radio_activity(uint32_t *NextStateSysTime)
{
  
  MASK_INTERRUPTS();
  *NextStateSysTime = next_wakeup_time;
  uint8_t ret = radio_sta;
  UNMASK_INTERRUPTS();

  return ret;
}
static uint8_t dumbConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  RADIO_TIME_UNIT;
  advPacket.WakeupTime = next_wakeup_time; 
  RADIO_SetReservedArea(&advPacket);
  select_adv_chan();
  access_address = ADV_ACCESS_ADDRESS;
  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
  return TRUE; 
}
static uint8_t dumbDataRoutine(ActionPacket* p,  ActionPacket* next)
{
  if(next == &advPacket){
    set_adv_data();
  }
  
  return TRUE;
}


static uint8_t advConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  RADIO_TIME_UNIT;
  rxConPacket.WakeupTime = next_wakeup_time; 
  RADIO_SetReservedArea(&rxConPacket);
  return TRUE;
}
static uint8_t advDataRoutine(ActionPacket* p,  ActionPacket* next)
{

 if(pcmd_block == ptx_empty_block)
  	ctl_msg_schedule(pmsg_ctl);
 
 if(memcmp(csta_all_dis, dev_csta, sizeof(dev_csta)) == 0){
    uint8_t ret = ctl_msg_schedule_and_clear(pmsg_ctl);
	if(ret == OS_ERR_NONE){
        pcmd_block = ptx_empty_block;
		cmdPacket.data = pcmd_block->buf;
	}
 }
 
  return TRUE;
}


static uint8_t check_connection_condction(ActionPacket* p)
{
  uint8_t ret = FALSE;
  
  if((p->data[2] == CMD_CONNECTION_REQ) && (p->data[1] == sizeof(con_req_t)) &&
  	(cur_free_con_handle !=0) ){
    return TRUE;
  }

  return ret;
}
/**
  * @brief  Condition routine.
  * @param  ActionPacket
  * @retval TRUE
  */
static uint8_t rxConConditionRoutine(ActionPacket* p)
{

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
      /* received a packet */
    if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
		 if(check_connection_condction(p))
		 {
		 	 next_wakeup_time +=  RADIO_TIME_UNIT; 
			 rspConPacket.WakeupTime = next_wakeup_time; 
			 RADIO_SetReservedArea(&rspConPacket);
			 return TRUE;  
		 }
      }
  }

  next_wakeup_time +=  2* RADIO_TIME_UNIT; 
  cmdPacket.WakeupTime = next_wakeup_time; 
  RADIO_SetReservedArea(&cmdPacket);
  update_con_chan();
  access_address = DEF_CON_ACCESS_ADDRESS;
  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
  return FALSE;
  
}


void prepare_cmdPacket(ActionPacket* p,  ActionPacket* next)
{
  if(next == &cmdPacket){
  	pcmd_block->buf[1] = sizeof(mcmd_t);
  	mcmd_t *pmcmd =  (mcmd_t*)(&pcmd_block->buf[2]);
	pmcmd->cmd = CMD_MASTER_TX_ALL;
  	pmcmd->head = mcmd_head;
    //mcmd_head;
  }  
}
static uint8_t rxConDataRoutine(ActionPacket* p,  ActionPacket* next)
{

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
      /* received a packet */
    if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
		 if((&rspConPacket == next) && (cur_free_con_handle != 0)){
		 	//COMPrintf_hexdump(p->data, p->data[1]+2);
            uint32_t pos = cur_free_con_handle -1;
            con_req_t *preq = (con_req_t*)(&p->data[2]);
			memcpy(dev[pos].dev_addr, preq->dev_addr, 6);
			
		 	con_rsp_t *pcon_rsp = (con_rsp_t*)(&pConrsp_block->buf[2]);
			pcon_rsp->con_handle = cur_free_con_handle;
			memcpy(pcon_rsp->src_dev_addr, bdaddr, sizeof(bdaddr));
			memcpy(pcon_rsp->dst_dev_addr, dev[pos].dev_addr, 6);
			pcon_rsp->cmd = CMD_CONNECTION_RSP;
			pConrsp_block->buf[1] = sizeof(con_rsp_t);

			if(cur_free_con_handle > 0 && cur_free_con_handle <= NODE_NUMBER){ 
				//((cur_free_con_handle%8 == 0)?0:1)
			  uint8_t byte_pos = (cur_free_con_handle-1)/8 ;
			  uint8_t byte_bit = (cur_free_con_handle-1)%8;
			  dev_csta[byte_pos] |= (1<<byte_bit);
			}

			onconnect_t *pcon;
			struct mem_msg_block_t *msg = mem_msg_get_block();
			if(msg != NULL){
				pcon = (onconnect_t *)msg->buf;
				pcon->con_handle = cur_free_con_handle;	
				memcpy(pcon->dev_addr, dev[pos].dev_addr, 6);
				msg_post(pmsg_report, radio_connection_complete_event, msg);
			}

		 }
      }
  }

  if(next == &cmdPacket){
     prepare_cmdPacket(p,next);
  }
  return TRUE;
}

static uint8_t rspConConditionRoutine(ActionPacket* p)
{
	next_wakeup_time +=  RADIO_TIME_UNIT; 
	cmdPacket.WakeupTime = next_wakeup_time; 
	RADIO_SetReservedArea(&cmdPacket);
	update_con_chan();
	access_address = DEF_CON_ACCESS_ADDRESS;
	RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);

    return TRUE;
}

static uint8_t rspConDataRoutine(ActionPacket* p,  ActionPacket* next)
{
  prepare_cmdPacket(p,next);
  return TRUE;
}

static uint8_t cmdConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  RADIO_TIME_UNIT;
  rxPacket[0].WakeupTime = next_wakeup_time; 
  RADIO_SetReservedArea(&rxPacket[0]);
  RADIO_SetMaxReceivedLength(STATE_MACHINE_0, MTU_SIZE);
  RADIO_SetGlobalReceiveTimeout(DEFUALT_RECEIVETIMEOUT);
  return TRUE;
}
static uint8_t cmdDataRoutine(ActionPacket* p,  ActionPacket* next)
{
  cur_node = 0;
 
  return TRUE;
}




void BLE_RXTX_SEQ_IRQHandler(void)
{
  if(LL_SYSCFG_BLERXTX_IsInterruptPending(LL_SYSCFG_BLE_TX_EVENT))
  {
    if(RRM->FSM_STATUS_DIG_OUT & RRM_FSM_STATUS_DIG_OUT_STATUS_4)
    {
    // Rising edge
      //COMPrintf("->");
      MASK_INTERRUPTS();
      if(radio_sta == STA_IDLE){
		radio_sta = STA_ACTIVE;
	  }
	  UNMASK_INTERRUPTS();
    }
    else
    {
    // Falling edge
      //COMPrintf("<-");
    }  
    // Do something
    LL_SYSCFG_BLERXTX_ClearInterrupt(LL_SYSCFG_BLE_TX_EVENT);
  }
  else if(LL_SYSCFG_BLERXTX_IsInterruptPending(LL_SYSCFG_BLE_RX_EVENT))
  {
    if(RRM->FSM_STATUS_DIG_OUT & RRM_FSM_STATUS_DIG_OUT_STATUS_4)
    {
    // Rising edge
    }
    else
    {
    // Falling edge
    }  
    // Do something
    LL_SYSCFG_BLERXTX_ClearInterrupt(LL_SYSCFG_BLE_RX_EVENT);
  }
}


/**
  * @brief  Condition routine.
  * @param  ActionPacket
  * @retval TRUE
  */
static uint8_t rxNodesConditionRoutine(ActionPacket* p)
{
  next_wakeup_time +=  RADIO_TIME_UNIT;
  rxPacket[(cur_node+1)%NODE_NUMBER].WakeupTime = next_wakeup_time;
  RADIO_SetReservedArea(&rxPacket[(cur_node+1)%NODE_NUMBER]);
  //p->WakeupTime = next_wakeup_time;
  
  if(cur_node >= (NODE_NUMBER-1)){
    cur_node = 0;
	
#ifdef RADIO_ACTIVE_MUTEX
	if(cur_event_num % RADIO_SKIP_PERIOD == 0){
       next_wakeup_time +=  ((NODE_NUMBER+RADIO_SMALL_SKIP_SLOT+3)*RADIO_TIME_UNIT);
	}else{
	   next_wakeup_time +=  (RADIO_SMALL_SKIP_SLOT*RADIO_TIME_UNIT);
	}
#else
	// nothing to do
#endif
    
	advPacket.WakeupTime = next_wakeup_time;  
	RADIO_SetReservedArea(&advPacket);
	cur_event_num++;
	select_adv_chan();
    access_address = ADV_ACCESS_ADDRESS;
    RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);

    MASK_INTERRUPTS();
    if(radio_sta == STA_ACTIVE){
	  radio_sta = STA_IDLE;
    }
    UNMASK_INTERRUPTS();
    
	return FALSE; 
  }
  else{
    //  
    
  }
  return TRUE;   
} 

uint32_t radio_rx_event_cnt  = 0;
static void report_rx_msg_to_app(ActionPacket* p,  ActionPacket* next)
{
	struct mem_trans_block_t *prx_block_bac = mem_trans_get_block();
	if(prx_block_bac != NULL){
	  radio_rx_event_cnt++;
	  
      //scmd_t *pscmd = (scmd_t*)(&p->data[2]);
	  //COMPrintf_hexdump(pscmd->buf,5);
	  
	  uint16_t ret = msg_post(pmsg_report, radio_rx_event, *(&(prx_block[cur_node])));
	  if(ret == OS_ERR_NONE){
		prx_block[cur_node] = prx_block_bac;
		rxPacket[cur_node].data = prx_block[cur_node]->buf;
		//COMPrintf("rxPacket[cur_node].data:%x \r\n", rxPacket[cur_node].data);
	  }
	  else{
	  	//COMPrintf("report_rx_msg_to_app err:%d ", ret);
        mem_trans_return_block(prx_block_bac);
	  }
	}
}


static void __do_connecting(ActionPacket* p,  ActionPacket* next)
{
  dev[cur_node].dis_con_count = 0;

  if(p->data[1] >= (sizeof(scmd_t) - USER_PACKET_SIZE) )
  {
    scmd_t *pscmd = (scmd_t*)(&p->data[2]);
	
	//COMPrintf_hexdump(pscmd->buf,5);
    //COMPrintf("pscmd->mNEST:%d \r\n", pscmd->head.mNextTransmitSeqNum);
	BOOL nesn = ( (pscmd->head.sTransmitSeqNum+1) % 2);
	// new rec massage
	if(get_bit(mcmd_head.nesn, UINT8_NODE_ARR, cur_node) != nesn){
	  if(nesn)
	    set_bit(mcmd_head.nesn, UINT8_NODE_ARR, cur_node);
	  else
	  	clear_bit(mcmd_head.nesn, UINT8_NODE_ARR, cur_node);

	  if(pscmd->buf_len != 0){
        report_rx_msg_to_app(p, next);
	  }
	}

	 // check all node
	uint16_t mNextTransmitSeqNum = pscmd->head.mNextTransmitSeqNum;
	if((pcmd_block != ptx_empty_block) &&
	  	(mNextTransmitSeqNum == mcmd_head.transmitSeqNum + 1))
	{
	  set_bit(dev_acked, UINT8_NODE_ARR, cur_node);
	}
	if(memcmp(dev_acked, dev_csta, sizeof(dev_acked)) == 0){
      if(pcmd_block != ptx_empty_block){
		pcmd_block = ptx_empty_block;
		cmdPacket.data = pcmd_block->buf;
		msg_post(pmsg_ctl_rsp, radio_tx_complete_event, cur_cmdmsg.param);
		memset(&dev_acked, 0, sizeof(dev_acked));
	  }
	}
	
  }
  
  
}

static uint8_t dev_is_connected(uint16_t node)
{
  uint8_t byte_pos = (node)/8 ;
  uint8_t byte_bit = (node)%8;
  return (dev_csta[byte_pos] & (1<<byte_bit));
}


static void __do_lost_connection_packet(ActionPacket* p,  ActionPacket* next)
{
  if(dev_is_connected(cur_node))
  {
	  dev[cur_node].dis_con_count++;
	  if(dev[cur_node].dis_con_count > DEFUALT_CONNECTION_TIMEOUT){
	  	uint8_t byte_pos = (cur_node)/8 ;
        uint8_t byte_bit = (cur_node)%8;
        dev_csta[byte_pos] &= (~(1<<byte_bit));
		
	    ondisconnect_t *pdis;
		struct mem_msg_block_t *msg = mem_msg_get_block();
		if(msg != NULL){
			pdis = (ondisconnect_t *)msg->buf;
			pdis->con_handle = cur_node+1;	
			memcpy(pdis->dev_addr, dev[cur_node].dev_addr, 6);
			msg_post(pmsg_report, radio_disconnection_complete_event, msg);
		}
	  }
  }
  

}

static uint8_t rxNodesDataRoutine(ActionPacket* p,  ActionPacket* next)
{

  if((p->status & BLUE_STATUSREG_PREVTRANSMIT) == 0) {
  /* received a packet */
    if((p->status & BLUE_INTERRUPT1REG_RCVOK) != 0) {
	  __do_connecting(p, next);
    }
    else{
	  __do_lost_connection_packet(p, next);
    }
  }
  // prepare for next node
  cur_node++;
    if(next == &advPacket){
    set_adv_data();
  }
  return TRUE;  
}

static void enable_BLE_RXTX_SEQ_IRQ(void)
{
  LL_APB0_EnableClock(LL_APB0_PERIPH_SYSCFG);
  
  LL_SYSCFG_BLERXTX_SetTrigger(LL_SYSCFG_BLERXTX_TRIGGER_BOTH_EDGE, LL_SYSCFG_BLE_TX_EVENT);
  LL_SYSCFG_BLERXTX_SetTrigger(LL_SYSCFG_BLERXTX_TRIGGER_BOTH_EDGE, LL_SYSCFG_BLE_RX_EVENT);
  
  LL_SYSCFG_BLERXTX_SetType(LL_SYSCFG_BLERXTX_DET_TYPE_EDGE, LL_SYSCFG_BLE_TX_EVENT);
  LL_SYSCFG_BLERXTX_SetType(LL_SYSCFG_BLERXTX_DET_TYPE_EDGE, LL_SYSCFG_BLE_RX_EVENT);
  LL_SYSCFG_BLERXTX_EnableIT(LL_SYSCFG_BLE_TX_EVENT|LL_SYSCFG_BLE_RX_EVENT);
  NVIC_EnableIRQ(BLE_SEQ_IRQn);
}

static void initDumbPacket(void)
{
  next_wakeup_time = HAL_VTIMER_GetCurrentSysTime();
  /* Build Action Packet */
  dumbPacket.StateMachineNo = STATE_MACHINE_0;
  dumbPacket.ActionTag = PLL_TRIG |TIMER_WAKEUP | TIMESTAMP_POSITION;
  // 
  next_wakeup_time += RADIO_TIME_UNIT;
  dumbPacket.WakeupTime = next_wakeup_time;				 
  dumbPacket.MaxReceiveLength = 0;					
  dumbPacket.data = padv_block->buf;				
  dumbPacket.next_true = &advPacket;				
  dumbPacket.next_false = &advPacket;				
  dumbPacket.condRoutine = dumbConditionRoutine;		  
  dumbPacket.dataRoutine = dumbDataRoutine;	
}

static void set_default_actionPacket(void)
{
  initDumbPacket();

  //next_wakeup_time = HAL_VTIMER_GetCurrentSysTime();
  /* Build Action Packet */
  advPacket.StateMachineNo = STATE_MACHINE_0;
  advPacket.ActionTag = PLL_TRIG | TXRX | TIMER_WAKEUP ;              
  advPacket.MaxReceiveLength = 0;                    
  advPacket.data = padv_block->buf;                          
  advPacket.next_true = &rxConPacket;               
  advPacket.next_false = &rxConPacket;              
  advPacket.condRoutine = advConditionRoutine;          
  advPacket.dataRoutine = advDataRoutine;               
  padv = (adv_packet_t*)(&padv_block->buf[2]);
  padv->discoverable = 1;
  memcpy(padv->dev_addr, bdaddr, sizeof(bdaddr));
  
  
  rxConPacket.StateMachineNo = STATE_MACHINE_0;
  rxConPacket.ActionTag = PLL_TRIG | TIMESTAMP_POSITION;             
  rxConPacket.MaxReceiveLength = MTU_SIZE;    
  rxConPacket.data = prxConreq_block->buf;                          
  rxConPacket.next_true = &rspConPacket;               
  rxConPacket.next_false = &cmdPacket;                       
  rxConPacket.condRoutine = rxConConditionRoutine;          
  rxConPacket.dataRoutine = rxConDataRoutine;              

  rspConPacket.StateMachineNo = STATE_MACHINE_0;
  rspConPacket.ActionTag = PLL_TRIG | TIMESTAMP_POSITION | TXRX;               
  rspConPacket.MaxReceiveLength = 0;   
  rspConPacket.data = pConrsp_block->buf;                           
  rspConPacket.next_true = &cmdPacket;              
  rspConPacket.next_false = &cmdPacket;                       
  rspConPacket.condRoutine = rspConConditionRoutine;          
  rspConPacket.dataRoutine = rspConDataRoutine;               

  cmdPacket.StateMachineNo = STATE_MACHINE_0;
  cmdPacket.ActionTag = PLL_TRIG | TXRX | TIMER_WAKEUP;             
  cmdPacket.MaxReceiveLength = 0;    
  cmdPacket.data = pcmd_block->buf;                        
  cmdPacket.next_true = &rxPacket[0];               
  cmdPacket.next_false = &rxPacket[0];                     
  cmdPacket.condRoutine = cmdConditionRoutine;          
  cmdPacket.dataRoutine = cmdDataRoutine;              

  for(int i=0;i<NODE_NUMBER-1;i++){
	  rxPacket[i].StateMachineNo = STATE_MACHINE_0;  
	  rxPacket[i].ActionTag = TIMER_WAKEUP | TIMESTAMP_POSITION;   
	  rxPacket[i].MaxReceiveLength = MTU_SIZE;   
	  rxPacket[i].data = prx_block[i]->buf;                       
	  rxPacket[i].next_true = &rxPacket[i+1];                  
	  rxPacket[i].next_false = &rxPacket[i+1];//&advPacket;                      
	  rxPacket[i].condRoutine = rxNodesConditionRoutine;          
	  rxPacket[i].dataRoutine = rxNodesDataRoutine; 
  }

  rxPacket[NODE_NUMBER-1].StateMachineNo = STATE_MACHINE_0;  
  rxPacket[NODE_NUMBER-1].ActionTag = TIMER_WAKEUP | TIMESTAMP_POSITION;   
  rxPacket[NODE_NUMBER-1].MaxReceiveLength = MTU_SIZE;   
  rxPacket[NODE_NUMBER-1].data = prx_block[NODE_NUMBER-1]->buf;                       
  rxPacket[NODE_NUMBER-1].next_true = &advPacket;                  
  rxPacket[NODE_NUMBER-1].next_false = &advPacket;                      
  rxPacket[NODE_NUMBER-1].condRoutine = rxNodesConditionRoutine;          
  rxPacket[NODE_NUMBER-1].dataRoutine = rxNodesDataRoutine; 

  

  /* Call this function before execute the action packet */
  RADIO_SetReservedArea(&dumbPacket);
  RADIO_SetReservedArea(&advPacket);
  RADIO_SetReservedArea(&rxConPacket);
  RADIO_SetReservedArea(&cmdPacket);
  for(int i=0;i<NODE_NUMBER;i++)
  	RADIO_SetReservedArea(&rxPacket[i]);
  
}


static void app_chan_init(void)
{
  uint8_t map[5] = DEFUALT_CON_CHAN_MAP;
  ull_chan_map_set(map);
}

static void update_con_chan(void)
{
  uint8_t ch_map[5];
  uint8_t chan_count = ull_chan_map_get(ch_map);
  channel = lll_chan_sel_2(cur_event_num, 0x305F, ch_map,chan_count); 
  RADIO_SetChannel(STATE_MACHINE_0, channel, 0);
}

void ctl_init(queue_msg_t *pctl, queue_msg_t *pctlrsp, queue_msg_t *preport)
{
  pmsg_ctl = pctl;
  pmsg_ctl_rsp = pctlrsp;
  pmsg_report = preport;
  

  for(int i=0; i<NODE_NUMBER; i++)
  {
    prx_block[i] = mem_trans_get_block();
  }
  padv_block = mem_trans_get_block();
  prxConreq_block = mem_trans_get_block();
  pConrsp_block = mem_trans_get_block();
  
  //pcmd_block = mem_trans_tx_get_block();
  ptx_empty_block = mem_trans_get_block();
  pcmd_block = ptx_empty_block;
  
  //mcmd_heard_t *p = ;

  
  generate_static_mac_addr(bdaddr);
  COMPrintf("mac:");
  COMPrintf_hexdump(bdaddr,sizeof(bdaddr));

  
  app_chan_init();
  
  enable_BLE_RXTX_SEQ_IRQ();

  set_default_actionPacket();
  
  /* Channel map configuration */
  uint8_t map[5]= {0xFF,0xFF,0xFF,0xFF,0xFF};
  RADIO_SetChannelMap(STATE_MACHINE_0, &map[0]);

  RADIO_SetGlobalReceiveTimeout(DEFUALT_RECEIVETIMEOUT);
  RADIO_SetMaxReceivedLength(STATE_MACHINE_0, MTU_SIZE);

  RADIO_SetPhy(STATE_MACHINE_0, PHY_2M);

  RADIO_SetBackToBackTime(BACK_TO_BACK_TIME);
  
  
  /* Setting of channel and the channel increment*/
  RADIO_SetChannel(STATE_MACHINE_0, channel, 0);

  /* Sets of the NetworkID and the CRC.*/
  RADIO_SetTxAttributes(STATE_MACHINE_0, access_address, 0x555555);
  
  /* Configures the transmit power level */
  RADIO_SetTxPower(MAX_OUTPUT_RF_POWER);
  LL_PWR_SetSMPSOutputLevel(LL_PWR_SMPS_OUTLVL_1V95);

  /* Call this function for the first action packet to be executed */
  radio_sta = STA_IDLE;
  RADIO_MakeActionPacketPending(&dumbPacket);
}


static uint8_t ctl_msg_schedule(queue_msg_t *msg_head)
{
  uint16_t ret = msg_post_accept(msg_head, &cur_cmdmsg);
  if (OS_ERR_NONE == ret){
	if (cur_cmdmsg.handler != NULL){
	  cur_cmdmsg.handler(cur_cmdmsg.param);
	}
  }

  return ret;
}

static uint8_t ctl_msg_schedule_and_clear(queue_msg_t *msg_head)
{
  uint16_t ret = msg_post_accept(msg_head, &cur_cmdmsg);
  if (OS_ERR_NONE == ret){
	if (cur_cmdmsg.handler != NULL){
	  cur_cmdmsg.handler(cur_cmdmsg.param);
	  if(mem_is_trans_range_addr(cur_cmdmsg.param)){
         mem_trans_return_one_tx_block((struct mem_trans_block_t *)cur_cmdmsg.param);
	  }
	  memset(&cur_cmdmsg, 0, sizeof(cur_cmdmsg));
	}
  }

  return ret;
}

void radio_tx(void* param)
{
  COMPrintf("radio_tx \r\n");
  struct mem_trans_block_t *pmsg = (struct mem_trans_block_t*)param;  
  pcmd_block = pmsg;
  cmdPacket.data = pcmd_block->buf;
  mcmd_head.transmitSeqNum++;
  pcmd_block->buf[1] = sizeof(scmd_t);
  mcmd_t *pmcmd = (mcmd_t*)(&pcmd_block->buf[2]);
  pmcmd->head = mcmd_head;
  
}



