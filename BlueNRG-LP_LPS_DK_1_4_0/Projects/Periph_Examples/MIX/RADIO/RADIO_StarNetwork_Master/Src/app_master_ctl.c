#include "app_master_ctl.h"
#include "qmsg.h"
#include "master_ctl.h"
#include "module.h"
#include "radio_event.h"
#include "cassert.h"
#include "ll_pdu.h"
#include "mem_mng.h"
#include "common_cfg.h"

#define MSG_TO_CTL_NUM	(10u)
#define MSG_REPORT_NUM	(NODE_NUMBER + 20u)

static queue_msg_t msg_to_ctl;
static queue_msg_t msg_ctl_rsp;
static queue_msg_t msg_report;

static msg_t _msg_to_ctl_grp[MSG_TO_CTL_NUM];
static msg_t _msg_ctl_rsp_grp[MSG_TO_CTL_NUM];

static msg_t _msg_report_grp[MSG_REPORT_NUM];


void app_ctl_init(void)
{
  msg_init(&msg_to_ctl, _msg_to_ctl_grp, MSG_TO_CTL_NUM);
  msg_init(&msg_ctl_rsp, _msg_ctl_rsp_grp, MSG_TO_CTL_NUM);
  msg_init(&msg_report, _msg_report_grp, MSG_REPORT_NUM);

  mem_init();
  

  ctl_init(&msg_to_ctl, &msg_ctl_rsp, &msg_report);
}

int app_radio_tx(uint8_t *pdata, uint8_t length)
{
  if((length > USER_PACKET_SIZE) || (length == 0))
  	return -1;

  int ret = 0;
  struct mem_trans_block_t* mem = mem_trans_tx_get_block();
  if(mem != NULL){
  	scmd_t *pscmd = (scmd_t*)(&mem->buf[2]);
	pscmd->cmd = CMD_SLAVE_TX;
	pscmd->buf_len =  length;
	memcpy(pscmd->buf, pdata, length);
    if(msg_post(&msg_to_ctl, radio_tx, mem) == OS_ERR_NONE)
		ret =0;
	else{
		ret = -3;
		mem_trans_return_one_tx_block(mem);
	}
  }else{
    ret = -2;
  }

  return ret;
}

void radio_tx_complete_event(void* param)
{
  COMPrintf("%s \r\n", __func__);
}



void app_updata_adv_data(uint8_t *pdata, uint8_t length)
{
  COMPrintf("%s \r\n", __func__);
}

void radio_connection_complete_event(void* param)
{
  COMPrintf("%s \r\n", __func__);  
}

void radio_disconnection_complete_event(void* param)
{
  COMPrintf("%s \r\n", __func__);
}


uint32_t app_event_cnt  = 0;
extern uint32_t radio_rx_event_cnt;

void radio_rx_event(void* param)
{
  //COMPrintf("%s \r\n", __func__);
  //COMPrintf("param:%x \r\n", param);
  struct mem_trans_block_t * pmem = (struct mem_trans_block_t *)param;
  scmd_t *pscmd = (scmd_t*)(&pmem->buf[2]);
  for(int i=0;i<10;i++)
  {
    COMPrintf("%x", pscmd->buf[i]);
  }
  //COMPrintf("\n");
  //COMPrintf_hexdump(pscmd->buf, 10);

  app_event_cnt++;

  COMPrintf("app_event_cnt:%d  radio_rx_event_cnt:%d  \r\n", app_event_cnt, radio_rx_event_cnt);
}







static uint8_t msg_schedule(queue_msg_t *msg_head)
{
	msg_t msg;
	memset(&msg, 0, sizeof(msg));
	
	uint16_t ret = msg_post_accept(msg_head, &msg);
	if (OS_ERR_NONE == ret){
		if (msg.handler != NULL){
			msg.handler(msg.param);
			// mem recycle
			if(mem_is_msg_range_addr(msg.param)){
				mem_msg_return_block((struct mem_msg_block_t *)msg.param);
			}else if(mem_is_trans_range_addr(msg.param)){
                mem_trans_return_block((struct mem_trans_block_t *)msg.param);
			}
		}
	}

  return ret;
}

static uint8_t ctl_rsp_msg_schedule(queue_msg_t *msg_head)
{
	msg_t msg;
	memset(&msg, 0, sizeof(msg));
	
	uint16_t ret = msg_post_accept(msg_head, &msg);
	if (OS_ERR_NONE == ret){
		if (msg.handler != NULL){
			msg.handler(msg.param);
			// mem recycle
			if(mem_is_trans_range_addr(msg.param)){
                mem_trans_return_one_tx_block((struct mem_trans_block_t *)msg.param);
			}
		}
	}

  return ret;
}

void app_ctl_schedule(void)
{
	uint8_t ret1, ret2;
	while(1)
	{
		ret1 = msg_schedule(&msg_report);
		ret2 = ctl_rsp_msg_schedule(&msg_ctl_rsp);
		if( (OS_ERR_NONE == ret1) || (OS_ERR_NONE == ret2)){
		   continue;
		}else{
		  break;
		}
	}

}


void test_app_radio_tx(void)
{
  uint8_t buf[64];
  int ret;
  static uint8_t scout = 0;
  scout++;
  for(int i=0;i<sizeof(buf);i++){
    buf[i] = scout;
  }
  
  ret = app_radio_tx(buf, sizeof(buf));
  
  if(ret != 0){
    COMPrintf("app_radio_tx:%d \r\n", ret);
  }
  #if 0
  for(int i=0;i<sizeof(buf);i++){
    buf[i] = 0x22;
  }
  ret = app_radio_tx(buf, sizeof(buf));
  if(ret != 0){
    COMPrintf("app_radio_tx:%d \r\n", ret);
  }
  for(int i=0;i<sizeof(buf);i++){
    buf[i] = 0x33;
  }
  ret = app_radio_tx(buf, sizeof(buf));
  if(ret != 0){
    COMPrintf("app_radio_tx:%d \r\n", ret);
  }
  #endif
}




module_init("APP",app_ctl_init);
task_register("APP", app_ctl_schedule, 0);

task_register("APP", test_app_radio_tx, 4*409600);




